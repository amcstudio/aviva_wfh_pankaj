'use strict';

~(function() {
    var $ = gsap,
        loops = 2,
        bgExit = document.getElementById('bgExit'),
        ad = document.getElementById('mainContent'),
        disc = document.getElementById('disc'),
        closeBtn = document.getElementById('closeLegalsBut'),
        pulseCount = 0,
        pulseAnimation;
       

    window.init = function() {
        bgExit.addEventListener('click', exitHandler);
        disc.addEventListener('click', openPannel);
        closeBtn.addEventListener('click', closePannel);
        ad.style.display = 'block';

        barAnimation();
        stairsAnimation();
        play();
            

    }

    function play() {
        
        var tl = gsap.timeline(); 
        tl.addLabel('frameOne')
        tl.to('#bgHadler', { duration: 4, scale: .9, rotation: 0.01, ease: 'power1.out' }, 'frameOne')
        tl.to('#header', { duration: 1, x: 0, opacity:1, rotation: 0.01, ease: 'power1.out' }, 'frameOne+=1')
        
        tl.addLabel('frameTwo', '-=1')
        tl.to('#OutLineOne', { duration: .5, scaleX: 1,  rotation: 0.01, ease: 'power1.inOut' }, 'frameTwo')
        tl.to('#OutLineOne', { duration: .5, opacity: 0, rotation: 0.01,  ease: 'power1.inOut' }, 'frameTwo+=.5')
        tl.to('#heroLineHandler', { duration: 3, scale: 1, rotation: 0.01, ease: "sine.inOut" }, 'frameTwo+=.5')
        tl.to('#topImg', { duration: .5, y: 0, yoyo: true, repeat: 1, repeatDelay: 3, rotation: 0.01, ease: 'power1.out' }, 'frameTwo+=.5')
        tl.to('#bottomImg', { duration: .5, y: -42, yoyo: true, repeat: 1, repeatDelay: 3, rotation: 0.01, ease: 'power1.out' }, 'frameTwo+=.5')

        tl.addLabel('frameThree','+=1')
        tl.to('#OutLineTwo', { duration: .5, scaleX: 1,  rotation: 0.01, ease: 'power1.inOut' }, 'frameThree')
        tl.to('#copyTopImg', { duration: .5, y: 0, rotation: 0.01, ease: 'power1.out' }, 'frameThree+=.5')
        tl.to('#copyBottomImg', { duration: .5, y: -118,  ease: 'power1.out' }, 'frameThree+=.5')
        
        tl.to('#cta', { duration: .5, opacity: 1,  ease: 'power1.out' }, 'frameThree+=1')
        tl.add(ctaPulse, 'frameThree+=1.5')
    }

    function ctaPulse() {
        doPulse();
        pulseAnimation = setInterval(() => {
            if(pulseCount < 2) {
                doPulse();
                pulseCount++;
            }
            else {
                clearInterval(pulseAnimation)
            }
        }, 1500);
    }

    function doPulse() {
        var tlPulse = gsap.timeline();
        tlPulse.set('#cta', { boxShadow: "0px 0px 0px 0px rgba(255, 217, 0, 1)" }), 
        tlPulse.to('#cta', { duration:1.25, boxShadow: "0px 0px 8px 20px rgba(255, 217, 0, 0)", ease: 'power4.out' })
    }

    function stairsAnimation() {
        var tlstrip = gsap.timeline();
        tlstrip.addLabel('stairsFill', '+=2.7');
        tlstrip.to('#stripeMask1', { duration: 5, height: 251, roation: 0.01, ease: "circ.out" }, 'stairsFill')
        tlstrip.to('#stripeMask2', { duration: 5, height: 251, roation: 0.01, ease: "circ.out" }, 'stairsFill+=.1')
        tlstrip.to('#stripeMask3', { duration: 5, height: 251, roation: 0.01, ease: "circ.out" }, 'stairsFill+=.2')
        tlstrip.to('#stripeMask4', { duration: 5, height: 251, roation: 0.01, ease: "circ.out" }, 'stairsFill+=.3')
        tlstrip.to('#stripeMask5', { duration: 5, height: 251, roation: 0.01, ease: "circ.out" }, 'stairsFill+=.4')
    }

    function barAnimation() {
        var bar = document.querySelectorAll('.vertBar'),
            adWidth = ad.clientWidth;
        for(var i=0; i<bar.length; i++ ) {
            var currentWidth = bar[i].clientWidth,
                newWidth = currentWidth * 2;
            bar[i].style.cssText = "transform: translateX("+adWidth+"px); width: "+ newWidth +"px";
        }

        var tlBar = gsap.timeline();
        tlBar.addLabel('moveBar');
        tlBar.to(['#vertBar_1','#vertBar_2'], { duration: .5, width: currentWidth, x: 0, rotation:0.01, stagger: 0.2, ease: "circ.out" },'moveBar');
        tlBar.to(['#vertBar_3'], { duration: .5, width: 3, x: 0, rotation:0.01, ease: "circ.out" },'moveBar+=.4');
        tlBar.to(['#vertBar_4','#vertBar_5','#vertBar_6'], { duration: .5, width: currentWidth, rotation:0.01, x: 0, stagger: 0.2, ease: "circ.out" },'moveBar+=.6');
    }

    function openPannel() {
        var tlPannel = gsap.timeline();
        tlPannel.to('#legalsPanel', {  duration: .5, y:0, ease: 'power4.out'})
        console.log("Open")
    }
    function closePannel() {
        var tlPannel = gsap.timeline();
        tlPannel.to('#legalsPanel', {  duration: .5, y:600, ease: 'power4.out'})
        console.log("Close")
    }
    function exitHandler(e) {
        e.preventDefault();
        Enabler.exit('Background Exit');
    }

})();